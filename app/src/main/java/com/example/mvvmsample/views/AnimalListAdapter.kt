package com.example.mvvmsample.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmsample.R
import com.example.mvvmsample.models.Animal
import com.example.mvvmsample.utils.getProgressDrawble
import com.example.mvvmsample.utils.loadImage
import kotlinx.android.synthetic.main.item_animal_list.view.*

class AnimalListAdapter (private val animalList : ArrayList<Animal> , private val callBack : CallBackAnimalListAdapter)
    : RecyclerView.Adapter<AnimalListAdapter.ViewHolder>(){

    private var choosen : Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_animal_list,parent,false))
    }

    override fun getItemCount(): Int {
        return animalList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(choosen == position)
        {
            holder.view.layout_animal.alpha = 0.6f
            choosen = position
        }
        else
            holder.view.layout_animal.alpha = 1f

        holder.view.txt_animal.text = animalList[position].name
        holder.view.img_animal.loadImage(animalList[position].image,
            getProgressDrawble(holder.view.context))
        holder.view.layout_animal.setOnClickListener(View.OnClickListener {
            callBack.goTo(animalList[position])
            choosen = position
            notifyDataSetChanged()
        })
    }

    fun updateAnimalList(newAnimalList : List<Animal>)
    {
        animalList.clear()
        animalList.addAll(newAnimalList)
        notifyDataSetChanged()
    }

    class ViewHolder(var view : View) : RecyclerView.ViewHolder(view)

    interface CallBackAnimalListAdapter
    {
        fun goTo(animal: Animal);
    }
}