package com.example.mvvmsample.views


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager

import com.example.mvvmsample.R
import com.example.mvvmsample.models.Animal
import com.example.mvvmsample.viewmodels.ListViewModel
import kotlinx.android.synthetic.main.fragment_list.*


class ListFragment : Fragment() , AnimalListAdapter.CallBackAnimalListAdapter {

    private lateinit var viewModel : ListViewModel
    private var adapterList: AnimalListAdapter = AnimalListAdapter(arrayListOf(),this)
    private var animalListDataObersver = Observer<List<Animal>>()
    {
        it?.let{
            list_animals.visibility = View.VISIBLE
            adapterList.updateAnimalList(it)
        }
    }
    private var loadingObersver = Observer<Boolean>()
    {
        loadingView.visibility = if(it) View.VISIBLE else View.GONE
        list_animals.visibility = if(it) View.GONE else list_animals.visibility
        txt_error.visibility = if(it) View.GONE else list_animals.visibility
    }
    private var errorObersver = Observer<Boolean>()
    {
        txt_error.visibility = if(it) View.VISIBLE else View.GONE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        viewModel.animals.observe(this,animalListDataObersver)
        viewModel.loading.observe(this,loadingObersver)
        viewModel.loadError.observe(this,errorObersver)
        viewModel.refresh()

        list_animals.apply {
            layoutManager = GridLayoutManager(context,2)
            adapter = adapterList
        }

        refresh.setOnRefreshListener {
            list_animals.visibility = View.GONE
            txt_error.visibility = View.GONE
            loadingView.visibility = View.VISIBLE
            viewModel.refresh()
            refresh.isRefreshing = false
        }

        btn_detail.visibility = View.GONE
    }

    override fun goTo(animal: Animal) {
        btn_detail.visibility = View.VISIBLE
        btn_detail.setOnClickListener(View.OnClickListener {
            val action = ListFragmentDirections.actionListFragmentToDetailsFragment(animal)
            Navigation.findNavController(it).navigate(action)
        })
    }
}
