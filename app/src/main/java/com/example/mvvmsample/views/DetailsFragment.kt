package com.example.mvvmsample.views


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation

import com.example.mvvmsample.R
import com.example.mvvmsample.databinding.FragmentDetailsBinding
import com.example.mvvmsample.models.Animal
import com.example.mvvmsample.utils.getProgressDrawble
import com.example.mvvmsample.utils.loadImage
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : Fragment() {

    var animal : Animal?= null
    private lateinit var dataBinding : FragmentDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_details,container,false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            animal = DetailsFragmentArgs.fromBundle(it).animal
            dataBinding.animal = animal
        }

        context?.let {
            dataBinding.imgAnimal.loadImage(animal?.image, getProgressDrawble(it))
//            img_animal.loadImage(animal?.image, getProgressDrawble(it))
        }

//        txt_animalName.text = animal?.name
//        txt_animalLoc.text = animal?.location
//        txt_animalLifespan.text = animal?.lifespan
//        txt_animalDiet.text = animal?.diet

        btn_list.setOnClickListener(View.OnClickListener {
            val action = DetailsFragmentDirections.actionDetailsFragmentToListFragment()
            Navigation.findNavController(it).navigate(action)
        })
    }


}
