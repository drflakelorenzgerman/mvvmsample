package com.example.mvvmsample.di

import com.example.mvvmsample.models.AnimalApiService
import com.example.mvvmsample.viewmodels.ListViewModel
import dagger.Component

@Component(modules = [ApiModule::class])
interface ApiComponent {
    fun inject(service : AnimalApiService)
    fun inject(viewModel : ListViewModel)
}