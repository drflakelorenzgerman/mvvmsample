package com.example.mvvmsample.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.mvvmsample.di.DaggerApiComponent
import com.example.mvvmsample.models.Animal
import com.example.mvvmsample.models.AnimalApiService
import com.example.mvvmsample.models.Apikey
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ListViewModel(appliaction : Application) : AndroidViewModel(appliaction){

    val animals by lazy { MutableLiveData<List<Animal>>() }
    val loadError by lazy { MutableLiveData<Boolean>() }
    val loading  by lazy { MutableLiveData<Boolean>() }

    private val disposable = CompositeDisposable()

    @Inject
    lateinit var apiService : AnimalApiService

    init
    {
        DaggerApiComponent.create().inject(this)
    }

    fun refresh()
    {
        loading.value = true
        getKey()
    }

    private fun getKey()
    {
        disposable.add(apiService.getApiKey().subscribeOn(Schedulers.newThread())
            .subscribeWith(object : DisposableSingleObserver<Apikey>() {
                override fun onSuccess(key: Apikey) {
                    if(key.key.isNullOrEmpty())
                    {
                        loadError.value = true
                        loading.value = false
                    }
                    else
                    {
                        getAnimals(key.key)
                    }
                }

                override fun onError(error: Throwable) {
                    error.printStackTrace()
                    loadError.value = true
                    loading.value = false
                }
            }))
    }

    private fun getAnimals(key : String)
    {
        disposable.add(apiService.getAnimals(key)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                it.filter {
                    it.name!!.toLowerCase().contains("o")
                }
            }
            .subscribe(
                {
                    loading.value = false
                    loadError.value = false
                    animals.value = it
                },
                {
                    it.printStackTrace()
                    loadError.value = true
                    loading.value = false
                }
            ))
//            .subscribeWith(
//                object : DisposableSingleObserver<List<Animal>>(){
//                override fun onSuccess(data: List<Animal>) {
//                    loading.value = false
//                    loadError.value = false
//                    animals.value = data
//                }
//                override fun onError(error: Throwable) {
//                    error.printStackTrace()
//                    loadError.value = true
//                    loading.value = false
//                }
//            }))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}