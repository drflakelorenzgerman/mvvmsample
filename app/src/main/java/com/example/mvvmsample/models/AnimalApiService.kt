package com.example.mvvmsample.models

import com.example.mvvmsample.di.DaggerApiComponent
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class AnimalApiService {


    @Inject
    lateinit var api : AnimalApi;

    init {
        DaggerApiComponent.create().inject(this)
    }


    fun getApiKey() : Single<Apikey>{
        return api.getApiKey()
    }

    fun getAnimals(key : String) : Observable<List<Animal>> {
        return api.getAnimals(key)
    }
}