package com.example.mvvmsample.models

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface AnimalApi {

    @GET("getKey")
    fun getApiKey() : Single<Apikey>

    @FormUrlEncoded
    @POST("getAnimals")
    fun getAnimals(@Field("key") key : String) : Observable<List<Animal>>

}